# Author:   Edmond Chuc
#
# Purpose:  1. Validate that each concept in data.ttl contains zero or one skos:broader 
#              relationships.
#           2. Validate that each concept's skos:prefLabel is unique across all the concepts in data.ttl.

import click
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import SKOS, RDF
from owlrl import DeductiveClosure, OWLRL_Semantics


def count_broaders(uri: URIRef, g: Graph) -> int:
    count = 0
    for broader in g.objects(uri, SKOS.broader):
        count += 1
    return count


def get_label(uri: URIRef, g: Graph) -> str:
    for label in g.objects(uri, SKOS.prefLabel):
        return label


def is_label_unique(label: str, g: Graph) -> bool:
    count = 0
    for prefLabel in g.objects(None, SKOS.prefLabel):
        if str(prefLabel) == str(label):
            count += 1
    if count > 1:
        return False
    return True


@click.command()
@click.argument('file', type=click.Path(exists=True))
def find(file):
    g = Graph().parse(file, format='turtle')
    g.parse('https://bitbucket.org/terndatateam/knowledge-graph-data/raw/HEAD/onto_rules/skos_rules.ttl', format='turtle')
    DeductiveClosure(OWLRL_Semantics).expand(g)

    errors = []

    for concept in g.subjects(RDF.type, SKOS.Concept):
        count = count_broaders(concept, g)
        if count > 1:
            errors.append('There is more than one broader on the concept {}'.format(concept))

        label = get_label(concept, g)
        unique = is_label_unique(label, g)
        if not unique:
            errors.append('The label {} is not unique on concept {}'.format(label, concept))

    if errors:
        for error in errors:
            print(error)
        raise Exception('Errors in turtle files. Total: {}'.format(len(errors)))


if __name__ == '__main__':
    find()
