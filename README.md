# Instruments Vocabulary

This SKOS controlled vocabulary describes the list of instruments for TERN Landscapes and EP Flux.

# Contact

**Jenny Mahuika**  
*Data librarian*  
[j.mahuika@uq.edu.au](mailto:j.mahuika@uq.edu.au)  


**Edmond Chuc**  
*Software engineer*  
[e.chuc@uq.edu.au](mailto:e.chuc@uq.edu.au)  
